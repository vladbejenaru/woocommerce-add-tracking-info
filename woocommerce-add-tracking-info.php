<?php
/*
Plugin Name:   WooCommerce Add Tracking Info
Plugin URI:    https://github.com/vladbejenaru/woocommerce-simple-tracking-info
Description:   Add tracking info to order and to customer emails
Version:       0.01
Author:        Vlad Bejenaru
Author URI:    http://bejenaru.net
*/

/*
 * Add tracking info after shipping details
 */


// disallow direct accesse

 if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * Check if WooCommerce is active
 **/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {


	//Add the tracking info fields in order dashboard view

	add_action( 'woocommerce_admin_order_data_after_shipping_address', 'trackinginfo_editable_order_meta_shipping' );

	function trackinginfo_editable_order_meta_shipping( $order ){

		$versandpartner	= get_post_meta ( $order->get_order_number(), 'versandpartner', true ) ;
		$tracking_nummer = get_post_meta ( $order->get_order_number(), 'tracking_nummer', true );
		$tracking_link = get_post_meta ( $order->get_order_number(), 'tracking_link', true ) ;

    if ( $tracking_link == '' ) {

        $tracking_link = 'https://tracking.dpd.de/status/en_US/parcel/';

    }

		woocommerce_wp_select( array(
			'id' => 'versandpartner',
			'label' => 'Versandpartner',
			'wrapper_class' => 'form-field-wide',
			'value' => $versandpartner,
			'options' => array(
				'DPD' => 'DPD', // option value == option name
				'Post.at' => 'Post.at'
			)
		) );

		woocommerce_wp_text_input( array(
			'id' => 'tracking_nummer',
			'label' => 'Tracking Nummer:',
			'value' => $tracking_nummer,
			'wrapper_class' => 'form-field-wide'
		) );

		woocommerce_wp_text_input( array(
			'id' => 'tracking_link',
			'label' => 'Tracking Link:',
			'value' => $tracking_link,
			'wrapper_class' => 'form-field-wide'
		) );

	}

	//Update the tracking info fields in order dashboard view

	add_action( 'woocommerce_process_shop_order_meta', 'trackinginfo_save_shipping_details' );


	function trackinginfo_save_shipping_details( $ord_id ){
		update_post_meta( $ord_id, 'versandpartner', wc_clean( $_POST[ 'versandpartner' ] ) );
		update_post_meta( $ord_id, 'tracking_nummer', wc_clean( $_POST[ 'tracking_nummer' ] ) );
		update_post_meta( $ord_id, 'tracking_link', wc_clean( $_POST[ 'tracking_link' ] ) );
		// update_post_meta( $ord_id, 'gift_message', wc_sanitize_textarea( $_POST[ 'gift_message' ] ) );
		// wc_clean() and wc_sanitize_textarea() are WooCommerce sanitization functions
	}


	// Add traking info to customer order email


	add_action( 'woocommerce_email_before_order_table', 'add_trackinginfo_to_email', 20 );

	function add_trackinginfo_to_email( $order ) {

		$versandpartner	= get_post_meta( $order->get_order_number(), 'versandpartner', true );
		$tracking_nummer = get_post_meta( $order->get_order_number(), 'tracking_nummer', true );
		$tracking_link = get_post_meta( $order->get_order_number(), 'tracking_link', true );

		if( $tracking_nummer <> '' ) {
            //Edit here the text from the emails

            //$title is used as secction title
            $title = "Bestellung unterwegs" ;

            //$text_confirming_shipping - simple text confirming shipment
            $text_confirming_shipping = "Ihre Sendung befindet sich nun auf dem Versandweg!" ;

            //$text_tracking_number - paragraph for displaying the tracking number
            $text_tracking_number = "Die Lieferschein-Nr. lautet: " ; //leave a space at the end!!!

            //$text_tracking_link - paragraph for displaying tracking link
            $text_tracking_link = "Die Sendung können Sie unter folgendem Link bei DPD weiterverfolgen: "; //leave a space at the end!!!

            // adding the actual tracking number to the text
            $text_tracking_number .= '<strong>' . $tracking_nummer . '</strong>. ';

            //adding the tracking link to the text
            if (  $tracking_link <> '' ) {

                // $text_tracking_link .= "<a href='" . $tracking_link . tracking_nummer . "'>" . $tracking_link . $tracking_nummer . "</a>" ;
                $text_tracking_link .= $tracking_link . $tracking_nummer  ;

            }

            ?>
			<table id="add-tracking-info" cellspacing="0" cellpadding="0" style="width: 100%; vertical-align: top; margin-bottom: 40px; margin-top: 20px; padding: 0; border= 2px solid black; border-collapse: collapse;">
				<tr>
					<td style="text-align: left; border: 0; padding: 0;" valign="top">
					   <hr>
                        <h2>
                            <?php esc_html_e( $title ) ; ?>
                        </h2>
                        <hr style="border: 0; border-top: 1px solid #8c8c8c; border-bottom: 1px solid #fff;">
                        <p>
                            <strong>
                                <?php esc_html_e( $text_confirming_shipping ) ; ?>
                            </strong>
                        </p>
                        <p>
                            <?php echo $text_tracking_number ; ?>
                        </p>

                        <?php if ( $tracking_link <> '' ) : ?>
                        <p>
                            <?php esc_html_e( $text_tracking_link ) ; ?>
                        </p>
                        <?php endif; ?>
                    <hr style="border: 0; border-top: 1px solid #8c8c8c; border-bottom: 1px solid #fff;">
                   	</td>
				</tr>
			</table>
			<?php
			}
	}

}
